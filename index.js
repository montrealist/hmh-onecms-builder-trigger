console.log('printing all env variables visible from the trigger job: ', process.env);

async function run() {

  let promise = new Promise((resolve, reject) => {
    setTimeout(() => resolve('trigger job is finished.'), 1000)
  });

  let result = await promise;

  console.log(result);
}

run();